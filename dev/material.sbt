<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <!-- Common base class for "real" and "void" materials-->
    <AttDef Type="material" BaseType="" Abstract="true" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="material.void" BaseType="material" Label="Void" Version="0">
      <ItemDefinitions>
        <Double Name="void-temperature" Label="Temperature">
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="sound-speed" Label="Speed of Sound" AdvanceLevel="1">
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="material.real" BaseType="material" Label="Material" Version="0">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Group Name="phases" Label="Phases" Extensible="true" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="name" Label="Name"></String>
            <Double Name="density" Label="Density (rho)" Optional="false">
              <BriefDescription>Mass density of the material phase</BriefDescription>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="density-deviation" Label="Density Deviation" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>
                The relative deviation of the true temperature-dependent
                density from the reference density
              </BriefDescription>
              <DefaultValue>0.0</DefaultValue>
              <!--ExpressionType fn.material.density-deviation-->
            </Double>
            <Double Name="conductivity" Label="Conductivity (K)">
              <BriefDescription>Thermal conductivity of the material phase</BriefDescription>
              <!--ExpressionType fn.material.conductivity-->
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="specific-heat" Label="Specific Heat (Cp)">
              <BriefDescription>Specific heat of the material phase</BriefDescription>
              <!--ExpressionType fn.material.specific-heat-->
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <!-- Omit Fluid property for now-->
          </ItemDefinitions>
        </Group>
        <Group Name="transitions" Label="Transitions" Extensible="true" NumberOfRequiredGroups="0">
          <ItemDefinitions>
            <Double Name="latent-heat" Label="Latent Heat (Lf)">
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="lower-transition-temperature" Label="Low Transition Temperature (Ts)">
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="upper-transition-temperature" Label="High Transition Temperature (Tl)">
              <DefaultValue>100.0</DefaultValue>
              <BriefDescription>The transition smoothing function. Default 0.25 times delta T.</BriefDescription>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
        <Double Name="smoothing-radius" Label="Smoothing Radius" NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false" Version="0"></Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Group" Name="Materials (Experimental)" TopLevel="true" TabPosition="North" FilterByAdvanceLevel="false" FilterByCategory="false">
      <Views>
        <View Title="Custom Group"/>
        <View Title="Default"/>
      </Views>
    </View>
    <View Type="Group" Title="Custom Group" Label="Custom" TabPosition="North">
      <Views>
        <View Title="Materials"/>
        <View Title="Assignment"/>
        <View Title="Void Material"/>
      </Views>
    </View>
    <View Type="smtkTruchasMaterialsView" Title="Materials">
      <AttributeTypes>
        <Att Name="material" Type="material"></Att>
      </AttributeTypes>
    </View>
    <View Type="Associations" Title="Assignment">
      <AttributeTypes>
        <Att Type="material"></Att>
      </AttributeTypes>
    </View>
    <View Type="Instanced" Title="Void Material" Label="Void Material">
      <InstancedAttributes>
        <Att Name="|Void Material" Type="material.void"></Att>
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Default" HideAssociations="true">
      <AttributeTypes>
        <Att Name="material" Type="material"></Att>
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeResource>
