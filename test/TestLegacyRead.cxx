//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

// Local incluides
#include "smtk/simulation/truchas/Registrar.h"
#include "smtk/simulation/truchas/operations/LegacyRead.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/VoidItem.h"
#include "smtk/model/Registrar.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/operation/operators/ReadResource.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/project/Registrar.h"
// #include "smtk/project/operators/Read.h"
#include "smtk/project/operators/Write.h"
#include "smtk/resource/Manager.h"
#include "smtk/session/vtk/Registrar.h"

#include "smtk/common/testing/cxx/helpers.h"

#include <boost/filesystem.hpp>

 #include <string>

const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

int TestLegacyRead(int /*argc*/, char* /*argv*/ [])
{
  boost::filesystem::path scratchPath(SCRATCH_DIR);
  boost::filesystem::path e2ePath = scratchPath / "end-to-end";

  // Check for legacy project
  const std::string projectName = "prob1a";
  boost::filesystem::path projectPath = e2ePath / projectName;
  smtkTest(boost::filesystem::exists(projectPath), "Legacy project not found at " << projectPath.string());

  // Delete .project.smtk file if found (to make test idempotent)
  std::string nprojectFilename = projectName + ".project.smtk";
  boost::filesystem::path nprojectFilePath = projectPath / nprojectFilename;
  if (boost::filesystem::exists(nprojectFilePath))
  {
    boost::filesystem::remove(nprojectFilePath);
  }

  boost::filesystem::path legacyFilePath = projectPath / ".smtkproject";
  smtkTest(boost::filesystem::exists(legacyFilePath), "Legacy project missing file " << legacyFilePath.string());

  // Initialize managers
  smtk::resource::ManagerPtr resManager = smtk::resource::Manager::create();
  smtk::attribute::Registrar::registerTo(resManager);
  smtk::model::Registrar::registerTo(resManager);
  smtk::session::vtk::Registrar::registerTo(resManager);

  smtk::operation::ManagerPtr opManager = smtk::operation::Manager::create();
  smtk::operation::Registrar::registerTo(opManager);
  smtk::attribute::Registrar::registerTo(opManager);
  smtk::session::vtk::Registrar::registerTo(opManager);

  smtk::project::ManagerPtr projectManager =
    smtk::project::Manager::create(resManager, opManager);
  smtk::project::Registrar::registerTo(projectManager);
  smtk::simulation::truchas::Registrar::registerTo(projectManager);

  // Todo Next line is currently a no-op. Is it needed?
  smtk::project::Registrar::registerTo(opManager);
  opManager->registerResourceManager(resManager);

  // Read the old-version project using LegacyRead operator
  {
    // Create reader operation and execute
    auto legacyReadOp = opManager->create<smtk::simulation::truchas::LegacyRead>();
    smtkTest(!!legacyReadOp, "Failed to create LegacyRead operation")

    legacyReadOp->parameters()->findFile("filename")->setValue(legacyFilePath.string());
    auto result = legacyReadOp->operate();
    int outcome = result->findInt("outcome")->value();
    std::cout << "Read Outcome: " << outcome << std::endl;
    if (outcome != OP_SUCCEEDED)
    {
      std::cout << legacyReadOp->log().convertToString() << std::endl;
    }
    smtkTest(outcome == OP_SUCCEEDED, "LegacyRead operation failed.");
    smtkTest(result->find("legacy")->isEnabled(), "Legacy flag is not set.");

    // Get the loaded project
    smtk::attribute::ResourceItemPtr projectItem = result->findResource("resource");
    smtk::resource::ResourcePtr resource = projectItem->value();
    auto project = std::dynamic_pointer_cast<smtk::project::Project>(resource);
    smtkTest(!!project, "Project not loaded.");
    smtkTest(!project->clean(), "Updated project not marked as modified.");

    // Write project
    auto writeOp = opManager->create<smtk::project::Write>();
    test(!!writeOp);

    writeOp->parameters()->associate(project);
    auto writeResult = writeOp->operate();
    int writeOutcome = writeResult->findInt("outcome")->value();
    smtkTest(outcome == OP_SUCCEEDED, "Write operation failed.");
    smtkTest(project->clean(), "Project still modified after writing to file system.");

    // Close project (apparently everything must be done explicitly, go figure)
    for (auto iter = project->resources().begin(); iter != project->resources().end(); ++iter)
    {
      resManager->remove(*iter);
    }
    resManager->remove(project);
    projectManager->remove(project);

    smtkTest(resManager->empty(), "Resource manager not empty after removing project.")
    project.reset();
  }

  // Read the project back in
  {
#if 0
    // Project read operator leaves project modified
    auto readOp = opManager->create<smtk::project::Read>();
#else
    // So use resource read op instead
    auto readOp = opManager->create<smtk::operation::ReadResource>();
#endif
    test(!!readOp);

    std::string projectFilename = projectName + ".project.smtk";
    boost::filesystem::path projectFilePath = projectPath / projectFilename;
    readOp->parameters()->findFile("filename")->setValue(projectFilePath.string());
    auto readResult = readOp->operate();
    int readOutcome = readResult->findInt("outcome")->value();
    smtkTest(readOutcome == OP_SUCCEEDED, "Project Read operation failed.");

    // Check the project
    smtk::attribute::ResourceItemPtr projectItem = readResult->findResource("resource");
    smtk::resource::ResourcePtr resource = projectItem->value();
    auto project = std::dynamic_pointer_cast<smtk::project::Project>(resource);
    smtkTest(!!project, "Project resource not read.");
    smtkTest(project->clean(), "Loaded project is modified.");

    smtkTest(project->resources().size() == 2, "Project has wrong number of resources.");

    // Get the resources
    smtk::attribute::ResourcePtr attResource;
    smtk::resource::ResourcePtr modelResource;
    for (auto res: project->resources().resources())
    {
      if (res->isOfType<smtk::attribute::Resource>())
      {
        attResource = std::dynamic_pointer_cast<smtk::attribute::Resource>(res);
      }
      else if (res->isOfType<smtk::model::Resource>())
      {
        modelResource = res;
      }
    } // for

    smtkTest(!!attResource, "Attribute resource missing from project.");
    smtkTest(!!modelResource, "Model resource missing from project.");

    // Verify they are associated
    auto resourceSet = attResource->associations();
    smtkTest(resourceSet.size() == 1, "Attribute resource missing associations.");
    auto res = *(resourceSet.begin());
    smtkTest(res == modelResource, "Model resource not associated to attribute resource.");
  }

  return 0;
}
