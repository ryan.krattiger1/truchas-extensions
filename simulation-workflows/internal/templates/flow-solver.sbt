<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Viscous Flow</Cat>
  </Categories>
  <Definitions>
    <!-- Flow Numerics-->
    <AttDef Type="flow-numerics" Label="Numerics">
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
      <ItemDefinitions>
        <Double Name="courant_number" Label="Courant Number">
          <BriefDescription>Sets an upper bound on the time step that is associated with stability of the explicit fluid advection algorithm.</BriefDescription>
          <DefaultValue>0.5</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Double Name="viscous_number" Label="Viscous Number" AdvanceLevel="1">
          <BriefDescription>Sets an upper bound on the time step that is associated with stability of an explicit treatment of viscous flow stress tensor.</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="vol_track_subcycles" Label="Volume Tracker Subcycles">
          <BriefDescription>Number of sub-time steps n taken by the volume tracker for every time step of the flow algorithm.</BriefDescription>
          <DefaultValue>2</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1</Min>
          </RangeInfo>
        </Int>
        <Void Name="nested_dissection" Label="Nested Dissection" Optional="true" IsEnabledByDefault="true" AdvanceLevel="1">
          <BriefDescription>Enables use of the nested dissection algorithm to reconstruct material interfaces in cells containing 3 or more materials.</BriefDescription>
        </Void>
        <Double Name="vol_frac_cutoff" Label="Volume Fraction Cutoff" AdvanceLevel="1">
          <BriefDescription>The smallest volume fraction allowed.</BriefDescription>
          <DefaultValue>1e-8</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Int Name="fischer_dim" Label="Fischer's Method Dimension" AdvancedLevel="1">
          <BriefDescription>The dimension of the subspace used in Fischer’s projection method for computing an initial guess for pressure projection system based on previous solutions.</BriefDescription>
          <DefaultValue>6</DefaultValue>
        </Int>
        <Double Name="fluid_frac_threshold" Label="Fluid Fraction Threshold" AdvanceLevel="1">
          <BriefDescription>Cells with a total fluid volume fraction less than this threshold are ignored by the flow solver.</BriefDescription>
          <DefaultValue>1e-2</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Double Name="min_face_fraction" Label="Min Face Fraction" AdvanceLevel="1">
          <BriefDescription>The minimum value of the fluid density associated with a face for the pressure projection system.</BriefDescription>
          <DefaultValue>1e-3</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Group Name="void_collapse" Label="Void Collapse" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <ItemDefinitions>
            <Double Name="void_collapse_relaxation" Label="Relaxation Parameter">
              <BriefDescription>The relaxation parameter in the void collapse model.</BriefDescription>
              <DefaultValue>0.1</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
                <Min Inclusive="true">1.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
        <Group Name="wisp_redistribution" Label="Wisp Redistribution" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <ItemDefinitions>
            <Double Name="wisp_cutoff" Label="Cutoff">
              <BriefDescription>Fluid cells with a fluid volume fraction below this value may be treated as wisps and havetheir fluid material moved around to other fluid cells.</BriefDescription>
              <DefaultValue>0.05</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="wisp_neighbor_cutoff" Label="Neighbor Cutoff">
              <BriefDescription>Fluid cells with a fluid volume fraction belowwisp_cutoffcan only be considered a wisp ifthe amount of fluid in the neighboring cells is also "small"</BriefDescription>
              <DefaultValue>0.25</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="material-priority" Label="Material Priority">
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="track_interfaces" Label="Track Interfaces" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>Enables the tracking of material interfaces.</BriefDescription>
          <ItemDefinitions>
            <Group Name="material_priority" Label="Material Priority" Extensible="true" NumberOfRequiredGroups="0">
              <BriefDescription>Priority order in which material interfaces are reconstructed within a cell for volume tracking.</BriefDescription>
              <ItemDefinitions>
                <Component Name="material" Label="Material" NumberOfRequiredValues="1">
                  <Accepts>
                    <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material']"></Resource>
                  </Accepts>
                </Component>
              </ItemDefinitions>
            </Group>
            <!-- Void item used by UI to track changes which require user confirmation-->
            <Void Name="confirmed" AdvanceLevel="99" Optional="true" IsEnabledByDefault="true"></Void>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="pressure-linear-solver" Label="Pressure Linear Solver">
      <Categories>
        <Cat>Fluid Flow</Cat>
      </Categories>
      <ItemDefinitions>
        <String Name="krylov_method" Label="Krylov Method" AdvanceLevel="1">
          <ChildrenDefinitions>
            <Int Name="krylov_dim" Label="Krylov Dimension" AdvanceLevel="1">
              <DefaultValue>5</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">1</Min>
              </RangeInfo>
            </Int>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Value>cg</Value>
            <Structure>
              <Value>gmres</Value>
              <Items>
                <Item>krylov_dim</Item>
              </Items>
            </Structure>
            <Value>bicgstab</Value>
          </DiscreteInfo>
        </String>
        <Double Name="conv_rate_tol" Label="Convergence Rate Tolerance" AdvanceLevel="1">
          <DefaultValue>0.9</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Double Name="abs_tol" Label="Absolute Tolerance">
          <DefaultValue>1e-9</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="rel_tol" Label="Relative Tolerance">
          <DefaultValue>0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="max_ds_iter" Label="Max diagonally scaled Krylov iterations">
          <DefaultValue>100</DefaultValue>
        </Int>
        <Int Name="max_pcg_iter" Label="Max preconditioned Krylov iterations">
          <DefaultValue>10</DefaultValue>
        </Int>
        <Void Name="verbose-output" Label="Verbose Output" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Sets HYPRE print level to 2 for debugging.</BriefDescription>
        </Void>
        <Group Name="hypre" Label="HYPRE Parameters" AdvanceLevel="1">
          <ItemDefinitions>
            <!-- Hide cg_use_two_norm (Oct 2020)-->
            <Void Name="cg_use_two_norm" Optional="true" IsEnabledByDefault="false" AdvanceLevel="99"></Void>
            <Double Name="amg_strong_threshold" Optional="true" IsEnabledByDefault="false"></Double>
            <Int Name="amg_max_levels" Optional="true" IsEnabledByDefault="false"></Int>
            <Int Name="amg_coarsen_method" Optional="true" IsEnabledByDefault="false"></Int>
            <Int Name="amg_smoothing_sweeps" Optional="true" IsEnabledByDefault="false"></Int>
            <Int Name="amg_smoothing_method" Optional="true" IsEnabledByDefault="false"></Int>
            <Int Name="amg_interp_method" Optional="true" IsEnabledByDefault="false"></Int>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="viscous-linear-solver" Label="Viscous Linear Solver">
      <Categories>
        <Cat>Viscous Flow</Cat>
      </Categories>
      <ItemDefinitions>
        <Group Name="implicit-time-stepping" Label="Implicit Time Stepping" Optional="true" IsEnabledByDefault="true">
          <ItemDefinitions>
            <Double Name="viscous_implicitness" Label="Viscous Implicitness" AdvanceLevel="1">
              <BriefDescription>The degree of time implicitness used for the velocity field in the discretization of the viscous flow stress tensor in the fluid momentum conservation equation.</BriefDescription>
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
            <String Name="krylov_method" Label="Krylov Method" AdvanceLevel="1">
              <ChildrenDefinitions>
                <Int Name="krylov_dim" Label="Krylov Dimension" AdvanceLevel="1">
                  <DefaultValue>5</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Value>cg</Value>
                <Structure>
                  <Value>gmres</Value>
                  <Items>
                    <Item>krylov_dim</Item>
                  </Items>
                </Structure>
                <Value>bicgstab</Value>
              </DiscreteInfo>
            </String>
            <Double Name="conv_rate_tol" Label="Convergence Rate Tolerance" AdvanceLevel="1">
              <DefaultValue>0.9</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
                <Max Inclusive="true">1.0</Max>
              </RangeInfo>
            </Double>
            <Double Name="abs_tol" Label="Absolute Tolerance">
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="rel_tol" Label="Relative Tolerance">
              <DefaultValue>0.000001</DefaultValue>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Int Name="max_ds_iter" Label="Max diagonally scaled Krylov iterations">
              <DefaultValue>100</DefaultValue>
            </Int>
            <Int Name="max_pcg_iter" Label="Max preconditioned Krylov iterations">
              <DefaultValue>10</DefaultValue>
            </Int>
            <Void Name="verbose-output" Label="Verbose Output" Optional="true" IsEnabledByDefault="false">
              <BriefDescription>Sets HYPRE print level to 2 for debugging.</BriefDescription>
            </Void>
            <Group Name="hypre" Label="HYPRE Parameters" AdvanceLevel="1">
              <ItemDefinitions>
                <!-- Hide cg_use_two_norm (Oct 2020)-->
                <Void Name="cg_use_two_norm" Optional="true" IsEnabledByDefault="false" AdvanceLevel="99"></Void>
                <Double Name="amg_strong_threshold" Optional="true" IsEnabledByDefault="false"></Double>
                <Int Name="amg_max_levels" Optional="true" IsEnabledByDefault="false"></Int>
                <Int Name="amg_coarsen_method" Optional="true" IsEnabledByDefault="false"></Int>
                <Int Name="amg_smoothing_sweeps" Optional="true" IsEnabledByDefault="false"></Int>
                <Int Name="amg_smoothing_method" Optional="true" IsEnabledByDefault="false"></Int>
                <Int Name="amg_interp_method" Optional="true" IsEnabledByDefault="false"></Int>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>