<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Views>
    <View Type="Group" Title="Fluid Flow" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="FF Boundary Conditions" />
        <View Title="FF Solver" />
        <View Title="FF Functions" />
      </Views>
    </View>

    <View Type="Attribute" Title="FF Boundary Conditions" Label="Boundary Conditions">
      <AttributeTypes>
        <Att Type="ff.boundary">
          <ItemViews>
            <View Item="velocity" FixedWidth="0" />
          </ItemViews>
        </Att>
      </AttributeTypes>
    </View>

    <View Type="Group" Title="FF Solver" Label="Solver" Style="GroupBox">
      <Views>
        <View Title="Flow" />
        <View Title="Pressure Linear Solver" />
        <View Title="Viscous Linear Solver" />
      </Views>
    </View>
    <View Type="Group" Title="Flow" Style="Tiled">
      <Views>
        <View Title="Flow Numerics" />
        <View Title="Material Priority" />
      </Views>
    </View>
    <View Type="Instanced" Title="Flow Numerics">
      <InstancedAttributes>
        <Att Type="flow-numerics" Name="flow-numerics" />
      </InstancedAttributes>
    </View>
    <View Type="smtkTruchasMaterialPriorityView" Title="Material Priority" Label=" ">
      <InstancedAttributes>
        <Att Type="material-priority" Name="material-priority" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Pressure Linear Solver">
      <InstancedAttributes>
        <Att Type="pressure-linear-solver" Name="pressure-linear-solver" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Viscous Linear Solver">
      <InstancedAttributes>
        <Att Type="viscous-linear-solver" Name="viscous-linear-solver" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="FF Functions" Label="Functions">
      <AttributeTypes>
        <Att Type="fn.ff"/>
      </AttributeTypes>
    </View>

  </Views>
</SMTK_AttributeSystem>
