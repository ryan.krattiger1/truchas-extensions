//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_truchas_project_ImportModel_h
#define smtk_simulation_truchas_project_ImportModel_h

#include "smtk/simulation/truchas/Exports.h"

#include "smtk/project/Operation.h"

namespace smtk
{
namespace simulation
{
namespace truchas
{

class SMTKTRUCHAS_EXPORT ImportModel : public smtk::project::Operation
{
public:
  smtkTypeMacro(smtk::simulation::truchas::ImportModel);
  smtkCreateMacro(ImportModel);
  smtkSharedFromThisMacro(smtk::operation::Operation);

protected:
  smtk::operation::Operation::Result operateInternal() override;
  smtk::operation::Operation::Specification createSpecification() override;
  virtual const char* xmlDescription() const override;
};
}
}
}

#endif
