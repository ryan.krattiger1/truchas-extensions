//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtSharedPropertiesItem - UI components for Truchas shared material properties
// .SECTION Description
// .SECTION See Also
// qtItem

#ifndef __smtk_extension_qtSharedPropertiesItem_h
#define __smtk_extension_qtSharedPropertiesItem_h

#include "smtk/simulation/truchas/qt/Exports.h"

#include "smtk/extension/qt/qtAttributeItemInfo.h"
#include "smtk/extension/qt/qtItem.h"

#include <QString>

#include <set>
#include <string>

class qtSharedPropertiesItemInternals;
class qtMaterialAttribute;

typedef smtk::extension::qtAttributeItemInfo qtItemInfo;

// Implements custom UI for Truchas shared-material properties, which
// is essentially a list of optional material property items that can
// be selected to represent all phases of the parent material.
// In several places, the code takes advantage of the fact that the
// shared properties are organized as a flat list of items.
class SMTKTRUCHASQTEXT_EXPORT qtSharedPropertiesItem : public smtk::extension::qtItem
{
  Q_OBJECT

public:
  static smtk::extension::qtItem* createItemWidget(
    const qtItemInfo& info, qtMaterialAttribute* attribute);
  qtSharedPropertiesItem(const qtItemInfo& info, qtMaterialAttribute* attribute);
  virtual ~qtSharedPropertiesItem();

  // Copies phase properties into shared properties and enables shared properties
  void copyPhaseItems(std::size_t phaseNumber);

signals:
  void itemEnabledStateChanged(QString itemName, bool isEnabled);

public slots:
  void onPhaseCountChanged();
  void updateItemData() override;

protected slots:
  void onItemModified();

protected:
  void createWidget() override;
  void buildItemFrame();

private:
  qtSharedPropertiesItemInternals* Internals;
}; // class

#endif
