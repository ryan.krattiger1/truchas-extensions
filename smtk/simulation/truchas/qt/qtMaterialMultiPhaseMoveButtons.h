#ifndef QTMOVEBUTTONS_H
#define QTMOVEBUTTONS_H

#include <QCommonStyle>
#include <QHBoxLayout>
#include <QPushButton>
#include <QWidget>

class qtMaterialMultiPhaseMoveButtons : public QWidget
{
  Q_OBJECT
  using Superclass = QWidget;

public:
  qtMaterialMultiPhaseMoveButtons(QWidget* parent = nullptr, int row = 0, bool is_lastRow = false);

  void upPushed();
  void downPushed();

  void setLastRow(bool lastRow);
  void setRow(int row);
  void setUpButtonVisibility(bool show);
  void setDownButtonVisibility(bool show);

signals:
  void moveUp(int row);
  void moveDown(int row);

private:
  void setButtonVisibility(QPushButton* button, bool show);
  int m_row;
  bool m_isLastRow;
  bool m_isPhase;
  QHBoxLayout* m_layout;
  QPushButton *m_upButton, *m_downButton;
};

#endif // QTMOVEBUTTONS_H
