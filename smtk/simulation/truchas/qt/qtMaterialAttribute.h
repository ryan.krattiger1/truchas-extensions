//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtMaterialAttribute - a class that encapsulates the UI of the
//       Truchas "material" Attribute
// .SECTION Description

#ifndef __smtk_extension_qtMaterialAttribute_h
#define __smtk_extension_qtMaterialAttribute_h

#include "smtk/simulation/truchas/qt/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/extension/qt/qtItem.h"
#include <QObject>
#include <QPointer>
#include <QWidget>

class qtMaterialAttributeInternals;
class QWidget;

namespace smtk
{
namespace extension
{
class qtItem;
}
}

class SMTKTRUCHASQTEXT_EXPORT qtMaterialAttribute : public QObject
{
  Q_OBJECT

public:
  qtMaterialAttribute(smtk::attribute::AttributePtr,
    const smtk::view::Configuration::Component& comp, QWidget* parent,
    smtk::extension::qtBaseView* view);
  virtual ~qtMaterialAttribute();

  smtk::attribute::AttributePtr attribute();
  QWidget* widget() { return m_widget; }
  QWidget* parentWidget();

  virtual void addItem(smtk::extension::qtItem*);
  QList<smtk::extension::qtItem*>& items() const;
  virtual void showAdvanceLevelOverlay(bool show);
  bool useSelectionManager() const { return m_useSelectionManager; }

  // A basic layout for an attribute
  void createBasicLayout(bool includeAssociations);

  // Returns true if it does not display any of its items
  bool isEmpty() const;

signals:
  // Signal indicates that the underlying item has been modified
  void modified();
  void itemModified(smtk::extension::qtItem*);

protected:
  virtual void createWidget();

  // Common logic for onAddPhase() and onRemovePhase()
  void updateItemData();

  QPointer<QWidget> m_widget;

protected slots:
  void onAddPhase();
  void onRemovePhase(int phaseNumber);
  void onItemModified();
  void onSharedStateChanged(QString itemName, bool isEnabled);

private:
  qtMaterialAttributeInternals* m_internals;
  bool m_useSelectionManager;
  bool m_isEmpty;
};

#endif
