//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtInductionSourceModel.h - Qt model for induction heating sources

#ifndef __smtk_simulation_truchas_qt_qtInductionSourceModel_h
#define __smtk_simulation_truchas_qt_qtInductionSourceModel_h

#include "smtk/PublicPointerDefs.h"

#include "smtk/simulation/truchas/qt/Exports.h"

#include <QAbstractTableModel>

class QMimeData;

/* **
  Qt table model for induction sources based on "induction-heating" attribute.
  The table has columns for time, frequency, coil 1, coil 2, etc., where the coil data
  are electrical current.

  The attribute contains an extensible group item named "source".
  Each subgroup contains two items:
    * Double item "time"
    * Int item "frequency"

  The attribute also contains an extensible group item named "coils". Each subgroup stores
  the geometry and current data for one induction coil.
 */

class SMTKTRUCHASQTEXT_EXPORT qtInductionSourceModel : public QAbstractTableModel
{
  Q_OBJECT
  typedef QAbstractTableModel Superclass;

public:
  qtInductionSourceModel(QWidget* parent, smtk::attribute::AttributePtr attribute);
  ~qtInductionSourceModel();

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  QVariant headerData(
    int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
  bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column,
    const QModelIndex& parent) override;

  // Rotate subgroup row to subgroup row-1
  bool rotateLeft(std::size_t row);

  // Reactions to external changes to attribute
  void beginAddCoil();
  void endAddCoil();
  void beginRemoveCoil(int row);
  void endRemoveCoil();
  bool beginRotateCoil(int fromCol, int toCol);
  void endRotateCoil();

  void beginAddTime();
  void endAddTime();
  void beginRemoveTime(int row);
  void endRemoveTime();

protected:
  smtk::attribute::AttributePtr m_attribute;
  smtk::attribute::GroupItemPtr m_sourceItem;
  smtk::attribute::GroupItemPtr m_coilsItem;
};

#endif
