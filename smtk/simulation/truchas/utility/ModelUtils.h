//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_truchas_utility_ModelUtils_h
#define smtk_simulation_truchas_utility_ModelUtils_h

#include "smtk/simulation/truchas/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/model/EntityTypeBits.h"

#include <string>

namespace smtk
{
namespace simulation
{
namespace truchas
{
class SMTKTRUCHAS_EXPORT ModelUtils
{
public:
  ModelUtils() = default;
  virtual ~ModelUtils() = default;

  // Assign colors to model entities
  bool assignColors(smtk::model::ResourcePtr modelResource, smtk::model::BitFlags entType,
    const std::vector<std::string>& palette) const;

  // Prefixes each model entity name with side set or element block number (using pedigree id)
  void renameModelEntities(smtk::model::ResourcePtr resource) const;

  // Assigns string properties to model resources, used by toolbar
  void tagResources(smtk::project::ProjectPtr project) const;

protected:
  void renameModelEntitiesInternal(smtk::model::ResourcePtr resource,
    smtk::model::EntityTypeBits entType, const std::string& prefix) const;
};
}
}
}

#endif
