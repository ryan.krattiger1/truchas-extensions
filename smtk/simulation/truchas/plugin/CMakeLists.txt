list(APPEND CMAKE_MODULE_PATH "${VTK_MODULES_DIR}")

set(header_files
  pqSMTKProjectCloseBehavior.h
  pqSMTKProjectLoader.h
  pqSMTKProjectOpenBehavior.h
  pqSMTKProjectSaveBehavior.h
  pqSMTKRecentProjectsMenu.h

  pqTruchasDisplayToolBar.h
  pqTruchasPluginLocation.h
  pqTruchasProjectExportBehavior.h
  pqTruchasProjectImportModelBehavior.h
  pqTruchasExtensionsAutoStart.h
  pqTruchasProjectMenu.h
  pqTruchasProjectNewBehavior.h
  )

set(qt_source_files
  pqSMTKProjectCloseBehavior.cxx
  pqSMTKProjectLoader.cxx
  pqSMTKProjectOpenBehavior.cxx
  pqSMTKProjectSaveBehavior.cxx
  pqSMTKRecentProjectsMenu.cxx

  pqTruchasDisplayToolBar.cxx
  pqTruchasPluginLocation.cxx
  pqTruchasExtensionsAutoStart.cxx
  pqTruchasProjectExportBehavior.cxx
  pqTruchasProjectImportModelBehavior.cxx
  pqTruchasProjectMenu.cxx
  pqTruchasProjectNewBehavior.cxx
  )

set(CMAKE_AUTOMOC 1)

# Auto start class
paraview_plugin_add_auto_start(
  CLASS_NAME pqTruchasExtensionsAutoStart
  INTERFACES auto_start_interfaces
  SOURCES auto_start_sources
  )

# Plugin location
paraview_plugin_add_location(
    CLASS_NAME pqTruchasPluginLocation
    INTERFACES location_interfaces
    SOURCES location_sources
  )

# Toolbar
paraview_plugin_add_toolbar(
    CLASS_NAME pqTruchasDisplayToolBar
    INTERFACES toolbar_interfaces
    SOURCES toolbar_sources
  )

# Add "Truchas" menu
paraview_plugin_add_action_group(
  CLASS_NAME pqTruchasProjectMenu
  GROUP_NAME "MenuBar/Truchas"
  INTERFACES menu_interfaces
  SOURCES menu_sources
  )

set(interfaces)
list(APPEND interfaces
    ${auto_start_interfaces}
    ${location_interfaces}
    ${menu_interfaces}
    ${toolbar_interfaces}
  )

set(interface_sources)
list(APPEND interface_sources
  ${auto_start_sources}
  ${location_sources}
  ${menu_interfaces}
  ${toolbar_sources}
  )

paraview_add_plugin(smtkTruchasPlugin
  VERSION "1.0"
  UI_INTERFACES
    ${interfaces}
  SOURCES
    ${header_files}
    ${view_source_files}
    ${qt_source_files}
    ${ui_files}
    ${interface_sources}
  UI_RESOURCES
    resources/pqTruchasDisplayToolBar.qrc
)

target_link_libraries(smtkTruchasPlugin
  LINK_PUBLIC
    smtkCore
    smtkQtExt
    smtkTruchas
    smtkPQComponentsExt
    smtkPVServerExt
    smtkTruchasQtExt
    ParaView::pqApplicationComponents
    ParaView::pqComponents
    ParaView::RemotingViews
    Qt5::Core
    Qt5::Widgets
)

target_include_directories(smtkTruchasPlugin PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  ${Boost_INCLUDE_DIRS}
)

target_compile_definitions(
  smtkTruchasPlugin PUBLIC RECENTLY_USED_PROJECTS_TAG="SMTK_PROJECT"
)
