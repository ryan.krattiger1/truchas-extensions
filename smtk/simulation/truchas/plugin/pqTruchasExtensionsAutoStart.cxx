//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/truchas/plugin/pqTruchasExtensionsAutoStart.h"

// Plugin includes
#include "smtk/simulation/truchas/Metadata.h"
#include "smtk/simulation/truchas/Registrar.h"
#include "smtk/simulation/truchas/qt/qtProjectRuntime.h"
#include "smtk/simulation/truchas/qt/qtTruchasViewRegistrar.h"

// SMTK includes
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtSMTKUtilities.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

#include <QDebug>

pqTruchasExtensionsAutoStart::pqTruchasExtensionsAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqTruchasExtensionsAutoStart::~pqTruchasExtensionsAutoStart()
{
}

void pqTruchasExtensionsAutoStart::startup()
{
  // Instantiate project runtime
  qtProjectRuntime::instance(pqCoreUtilities::mainWidget());

  // Check for current/active pqServer
  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server != nullptr)
  {
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
    this->resourceManagerAdded(wrapper, server);
  }

  // Listen for server connections
  auto smtkBehavior = pqSMTKBehavior::instance();
  QObject::connect(smtkBehavior, static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
                                   &pqSMTKBehavior::addedManagerOnServer),
    this, &pqTruchasExtensionsAutoStart::resourceManagerAdded);
  QObject::connect(smtkBehavior, static_cast<void (pqSMTKBehavior::*)(pqSMTKWrapper*, pqServer*)>(
                                   &pqSMTKBehavior::removingManagerFromServer),
    this, &pqTruchasExtensionsAutoStart::resourceManagerRemoved);
}

void pqTruchasExtensionsAutoStart::shutdown()
{
}

void pqTruchasExtensionsAutoStart::resourceManagerAdded(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::resource::ManagerPtr resManager = wrapper->smtkResourceManager();
  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  smtk::operation::ManagerPtr opManager = wrapper->smtkOperationManager();
  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  if (!resManager || !viewManager || !opManager || !projManager)
  {
    return;
  }

  smtk::simulation::truchas::Registrar::registerTo(projManager);
  smtk::extension::qtTruchasViewRegistrar::registerTo(viewManager);
}

void pqTruchasExtensionsAutoStart::resourceManagerRemoved(pqSMTKWrapper* wrapper, pqServer* server)
{
  if (!wrapper || !server)
  {
    return;
  }

  smtk::view::ManagerPtr viewManager = wrapper->smtkViewManager();
  if (viewManager != nullptr)
  {
    smtk::extension::qtTruchasViewRegistrar::unregisterFrom(viewManager);
  }

  smtk::project::ManagerPtr projManager = wrapper->smtkProjectManager();
  if (projManager != nullptr)
  {
    smtk::simulation::truchas::Registrar::unregisterFrom(projManager);
  }
}
