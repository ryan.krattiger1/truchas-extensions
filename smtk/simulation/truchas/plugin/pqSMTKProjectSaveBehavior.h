//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectSaveBehavior_h
#define pqSMTKProjectSaveBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqSMTKProjectSaveReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqSMTKProjectSaveReaction(QAction* parent);

  void saveProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->saveProject(); }

private:
  Q_DISABLE_COPY(pqSMTKProjectSaveReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKProjectSaveBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKProjectSaveBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKProjectSaveBehavior() override;

protected:
  pqSMTKProjectSaveBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectSaveBehavior);
};

#endif
