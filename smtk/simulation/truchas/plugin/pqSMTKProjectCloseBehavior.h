//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectCloseBehavior_h
#define pqSMTKProjectCloseBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqSMTKProjectCloseReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqSMTKProjectCloseReaction(QAction* parent);

  void closeProject();

signals:
  void projectClosed();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->closeProject(); }

private:
  Q_DISABLE_COPY(pqSMTKProjectCloseReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKProjectCloseBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKProjectCloseBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKProjectCloseBehavior() override;

protected:
  pqSMTKProjectCloseBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectCloseBehavior);
};

#endif
